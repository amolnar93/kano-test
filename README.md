# README #
First of all, I would like to thank for the opportunity to complete the exercise.

It took me 2 hours and 30 minutes to finish this test.

I chose Java to complete the tasks since this is the language I am most comfortable with.
You can import and build this project with Maven.
I used the JUnit testing framework to make assertions, run and verify my tests.
Besides Java, I used the Selenium automation framework with two webdrivers: chromedriver and geckodriver.

## [Test 1 and Test 2](src/test/java/Test_1_2.java) ##
I created Page Object Models and implemented driver commands for the [Home Page](src/main/java/pages/kano/HomePage.java), [Computer Kit Bundle Page](src/main/java/pages/kano/ComputerKitBundlePage.java) and [Cart Page](src/main/java/pages/kano/CartPage.java).
I completed this test with chromedriver because it allowed me to set implicit wait for the page to load and I found it more stable than Firefox.

## [Test 3](src/test/java/Test_3.java) ##
I created Page Object Factories for this test in [Projects Page](src/main/java/pages/world/ProjectsPage.java) since I had to verify that page elements are present.
In this test, I was asked to check whether `Connected Kanos`, `Online Today`, `Lines of code` and `Creations shared` information is present in the footer.
`Online Today` was not on the page when writing the test, so it will fail and I am aware of that.
Unfortunately, I faced some issues when trying to complete Test 3 with chromedriver since it did not handle `shadow-root` elements well (I executed javascript commands first, but it did not return the correct elements).
I completed this task with geckodriver since Firefox does not have native `shadow-root` support yet, so I could access these elements and their children with the `findElement()` function easily.

## Test 4 ##
I have started working on this test but ran out of time. 
I went through the onboarding journey manually and found out that I could verify the behaviour with selenium advanced actions like drag and drop for the [Onboarding Page](src/main/java/pages/world/OnboardingPage.java) with offset positions. I used POM here as well.

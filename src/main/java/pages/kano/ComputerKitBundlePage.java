package pages.kano;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ComputerKitBundlePage {
    private WebDriver driver;
    private By kitBundleCurrency = By.cssSelector(".BundleDisplay-priceCurrency");
    private By kitBundlePrice = By.cssSelector("span.BundleDisplay-price");
    private By kitBundleName = By.className("BundleDisplay-title");
    private By buyNowButton = By.id("BundleDisplay-actionPrimary--kano-complete");

    public ComputerKitBundlePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getKitBundlePrice() {
        return driver.findElement(kitBundleCurrency).getText() + driver.findElement(kitBundlePrice).getText();
    }

    public String getKitBundleName() {
        return driver.findElement(kitBundleName).getText();
    }

    public void clickBuyNowButton() {
        driver.findElement(buyNowButton).click();
    }
}

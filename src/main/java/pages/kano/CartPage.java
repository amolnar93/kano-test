package pages.kano;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    private WebDriver driver;
    private By itemPrice = By.className("CartPage-itemPrice");
    private By itemName = By.className("CartPage-itemNameLink");
    private By quantity = By.cssSelector(".CartPage-quantityValue.CartPage-quantityValue--valid");
    private By totalPrice = By.cssSelector(".CartPage-summaryItemValue.CartPage-summaryItemValue--total");

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getItemPrice() {
        return driver.findElement(itemPrice).getText();
    }

    public String getItemName() {
        return driver.findElement(itemName).getText();
    }

    public String getQuantity() {
        return driver.findElement(quantity).getAttribute("value");
    }

    public String getTotalPrice() {
        return driver.findElement(totalPrice).getText();
    }
}

package pages.kano;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private WebDriver driver;
    private By computerKitBundle = By.cssSelector("a[href*='kano-complete']");
    private By currentCountryDropDown = By.cssSelector(".current-region.style-scope.kano-cart");
    private By countryListElement = By.cssSelector(".region.region--active.style-scope.kano-cart");
    private By countryChoosePopupCloseElement = By.className("LocationModal-close");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickComputerKitBundle() {
        driver.findElement(computerKitBundle).click();
    }

    public void clickCountryDropDown() {
        driver.findElement(currentCountryDropDown).click();
    }

    public void clickCountryListElement() {
        driver.findElement(countryListElement).click();
    }

    public void closeCountryChoosePopup() {
        driver.findElement(countryChoosePopupCloseElement).click();
    }
}

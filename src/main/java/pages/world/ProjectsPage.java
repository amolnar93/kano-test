package pages.world;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProjectsPage {
    private WebDriver driver;

    @FindBy(css = "a[href*='/shares']")
    private WebElement worldMenuElement;

    @FindBy(css = ".menu-item > a[href*='/projects']")
    private WebElement makeMenuElement;

    @FindBy(css = ".menu-item > a[href*='/store']")
    private WebElement shopMenuElement;

    @FindBy(css = ".auth-menu > li:nth-child(1) > div:nth-child(1)")
    private WebElement loginMenuElement;

    @FindBy(css = ".auth-menu > li:nth-child(2) > div:nth-child(1)")
    private WebElement signUpMenuElement;

    @FindBy(xpath = "//p[text()=\"Connected Kanos\"]")
    private WebElement connectedKanosTextElement;

    @FindBy(xpath = "//p[text()=\"Online Today\"]")
    private WebElement onlineTodayTextElement;

    @FindBy(xpath = "//p[text()=\"Lines of code\"]")
    private WebElement linesOfCodeTextElement;

    @FindBy(xpath = "//p[text()=\"Creations shared\"]")
    private WebElement creationsSharedTextElement;

    public ProjectsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getWorldMenuElement() {
        return worldMenuElement;
    }

    public WebElement getMakeMenuElement() {
        return makeMenuElement;
    }

    public WebElement getShopMenuElement() {
        return shopMenuElement;
    }

    public WebElement getLoginMenuElement() {
        return loginMenuElement;
    }

    public WebElement getSignUpMenuElement() {
        return signUpMenuElement;
    }

    public WebElement getConnectedKanosTextElement() {
        return connectedKanosTextElement;
    }

    public WebElement getOnlineTodayTextElement() {
        return onlineTodayTextElement;
    }

    public WebElement getLinesOfCodeTextElement() {
        return linesOfCodeTextElement;
    }

    public WebElement getCreationsSharedTextElement() {
        return creationsSharedTextElement;
    }
}

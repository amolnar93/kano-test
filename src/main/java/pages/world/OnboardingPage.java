package pages.world;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class OnboardingPage {

    private WebDriver driver;
    private By sayBlock = By.cssSelector("g.blocklyDraggable:nth-child(1)");
    private By superPowersBlock = By.cssSelector("g.blocklyDraggable:nth-child(2)");

    public OnboardingPage(WebDriver driver) {
        this.driver = driver;
    }

    public void moveSayBlockToSuperPowersBlock() {
        Actions builder = new Actions(driver);
        Action dragAndDrop = builder.clickAndHold(driver.findElement(sayBlock))
                .moveToElement(driver.findElement(superPowersBlock))
                .release()
                .build();

        dragAndDrop.perform();
    }
}

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.kano.CartPage;
import pages.kano.ComputerKitBundlePage;
import pages.kano.HomePage;

import java.util.concurrent.TimeUnit;

public class Test_1_2 {
    private WebDriver driver;
    private HomePage homePage;
    private ComputerKitBundlePage computerKitBundlePage;
    private CartPage cartPage;

    @Before
    public void setup() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://kano.me");
    }

    @Test
    public void Computer_Kit_Bundle_Is_Added_To_The_Basket_With_The_Correct_Information() {
        homePage = new HomePage(driver);
        homePage.clickComputerKitBundle();

        computerKitBundlePage = new ComputerKitBundlePage(driver);
        String kitBundlePrice = computerKitBundlePage.getKitBundlePrice();
        String kitBundleName = computerKitBundlePage.getKitBundleName();
        computerKitBundlePage.clickBuyNowButton();

        cartPage = new CartPage(driver);
        Assert.assertTrue(cartPage.getItemName().equals(kitBundleName));
        Assert.assertTrue(cartPage.getItemPrice().replaceAll("\"", "").equals(kitBundlePrice));
        Assert.assertTrue(cartPage.getQuantity().equals("1"));
        Assert.assertTrue(cartPage.getTotalPrice().replaceAll("\"", "").equals(kitBundlePrice));
    }

    @Test
    public void Change_To_Another_Country_Computer_Kit_Bundle_Is_Added_To_The_Basket_With_The_Correct_Information() {
        homePage = new HomePage(driver);
        homePage.clickCountryDropDown();
        homePage.clickCountryListElement();
        homePage.closeCountryChoosePopup();

        Computer_Kit_Bundle_Is_Added_To_The_Basket_With_The_Correct_Information();
    }

    @After
    public void Close() {
        driver.close();
    }
}
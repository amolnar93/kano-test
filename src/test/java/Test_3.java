import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.world.ProjectsPage;

public class Test_3 {
    private WebDriver driver;
    private ProjectsPage projectsPage;

    @Before
    public void setup() throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("https://world.kano.me/projects");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("nav")));
        projectsPage = new ProjectsPage(driver);
    }

    @Test
    public void Check_That_Menu_Is_Correct() {
        Assert.assertTrue(projectsPage.getWorldMenuElement().isDisplayed());
        Assert.assertTrue(projectsPage.getMakeMenuElement().isDisplayed());
        Assert.assertTrue(projectsPage.getShopMenuElement().isDisplayed());
        Assert.assertTrue(projectsPage.getLoginMenuElement().isDisplayed());
        Assert.assertTrue(projectsPage.getSignUpMenuElement().isDisplayed());
    }

    @Test
    public void Check_That_The_Footer_Is_Correct() {
        Assert.assertTrue(projectsPage.getConnectedKanosTextElement().isDisplayed());

        //The test will fail at this point, you can comment the next line to make the test pass.
        Assert.assertTrue(projectsPage.getOnlineTodayTextElement().isDisplayed());
        Assert.assertTrue(projectsPage.getLinesOfCodeTextElement().isDisplayed());
        Assert.assertTrue(projectsPage.getCreationsSharedTextElement().isDisplayed());
    }

    @After
    public void Close() {
        driver.quit();
    }
}
